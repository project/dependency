<?php

/**
 * @file
 * Admin page callbacks for Dependency module.
 */

/**
 * Form builder: Configure entity dependencies.
 */
function dependency_settings_form($form) {
  $form['dependency_rules'] = array(
    '#type' => 'textarea',
    '#title' => t('Dependency rules'),
    '#default_value' => variable_get('dependency_rules', ''),
    '#rows' => 10,
    '#description' => t('Rules for entity dependencies. See the rule syntax below.'),
  );

  $syntax_help = array(
    'entity_type:field_name->entity_type',
    'entity_type(bundle):field_name->entity_type(bundle)',
    'entity_type(bundle)<-field_name:entity_type(bundle)',
    'entity_type(bundle1, bundle2)<-field_name1:entity_type(bundle):field_name2->entity_type',
    '',
    'The leftmost entity is the entity on which the rightmost entity depends.',
    'Dependency chains can contains several field dependencies which use reference fields.',
    'Field names are placed at the entity which contains these fields, they are separated from it by colons.',
    'The arrow points from the field name to the referenced entity.',
    'Bundle restriction lists can contain several bundle names or be empty.',
    'Lines started with # sign are comments.',
    'Optional section definition: [tag1, tag2].',
    '',
    'Examples:',
    '[search_index] - starts a section tagged as "search_index" for the following lines',
    'taxonomy_term(tags)<-field_tag:node(article) - node depends on taxonomy term',
    'node(article):field_author->user - user depends on node',
  );

  $form['dependency_help'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Syntax of dependency rules'),
    '#description' => implode('<br />', $syntax_help),
  );

  $form['dependency_invoke_update_hooks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Invoke update hooks for dependent entities'),
    '#description' => t('Invoke hook_{entity_name}_update and hook_entity_update hooks for dependent entities when an entity on which they depend is changed.'),
    '#default_value' => variable_get('dependency_invoke_update_hooks', 0),
  );

  $form['dependency_update_hooks_cron_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Use cron to invoke update hooks if count of dependent entities is more than'),
    '#options' => array(
      '10' => '10',
      '20' => '20',
      '50' => '50',
      '100' => '100',
      '200' => '200',
      '500' => '500',
    ),
    '#default_value' => variable_get('dependency_update_hooks_cron_threshold', '10'),
    '#states' => array(
      'visible' => array(
        ':input[name="dependency_invoke_update_hooks"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['#validate'][] = 'dependency_settings_form_validate_rules';
  $form['#submit'][] = 'dependency_settings_form_clear_cache';

  return system_settings_form($form);
}

/**
 * Form submission handler for the dependency_settings_form() form.
 */
function dependency_settings_form_clear_cache($form, &$form_state) {
  cache_clear_all('dependency:rules', 'cache');
}

/**
 * Validator for the dependency_settings_form() form.
 */
function dependency_settings_form_validate_rules($form, &$form_state) {
  module_load_include('inc', 'dependency', 'includes/dependency.compile');
  if (is_null(dependency_rules_compile($form_state['values']['dependency_rules']))) {
    form_set_error('dependency_rules', t('Incorrect syntax of dependendy rules.'));
  }
}
