<?php

/**
 * @file
 * Dependency module process functions.
 */

/**
 * Apply rules to updated entity.
 */
function dependency_process_apply_rules($entity_type, $entity, array $rules, $operation) {
  list($entity_id, , $bundle) = entity_extract_ids($entity_type, $entity);

  $dependent_entities = array();

  foreach ($rules as $tag => $tag_rules) {
    foreach ($tag_rules as $rule) {
      if (!isset($rule[0]['entity'])) {
        continue;
      }
      if ($entity_type !== $rule[0]['entity']) {
        continue;
      }
      if (!empty($rule[0]['bundles']) && !in_array($bundle, $rule[0]['bundles'])) {
        continue;
      }
      dependency_process_find_dependent_by_rule($entity, $entity_id, $rule, $tag, $dependent_entities);
    }
  }

  drupal_static_reset('dependency_dependent_ids');
  drupal_static_reset('dependency_dependent_ids_by_rule');

  if (empty($dependent_entities)) {
    return;
  }

  foreach ($dependent_entities as $tag => $tag_dependent_entities) {
    // Do not work with the original updated entity.
    unset($tag_dependent_entities[$entity_type][$entity_id]);

    foreach ($tag_dependent_entities as $dependent_entity_type => $dependent_entity_ids) {
      $context = array(
        'entity_type' => $entity_type,
        'entity' => $entity,
        'operation' => $operation,
      );

      module_invoke_all('dependent_entity_notify', $dependent_entity_type, array_values($dependent_entity_ids), $tag, $context);
    }
  }

  if (variable_get('dependency_invoke_update_hooks', 0)) {
    $dependent_entities_by_type = array();

    foreach ($dependent_entities as $tag => $tag_dependent_entities) {
      // Do not work with the original updated entity.
      unset($tag_dependent_entities[$entity_type][$entity_id]);

      foreach ($tag_dependent_entities as $dependent_entity_type => $dependent_entity_ids) {
        foreach ($dependent_entity_ids as $id) {
          $dependent_entities_by_type[$dependent_entity_type][$id] = $id;
        }
      }
    }

    $dependent_entity_count = 0;

    foreach ($dependent_entities_by_type as $dependent_entity_ids) {
      $dependent_entity_count += count($dependent_entity_ids);
    }

    if ($dependent_entity_count > intval(variable_get('dependency_update_hooks_cron_threshold', '10'))) {
      dependency_process_enqueue_dependent_entities($dependent_entities_by_type);
    }
    else {
      dependency_process_invoke_entity_update_hooks($dependent_entities_by_type);
    }
  }
}

/**
 * Find dependent entities using the rule applied to updated entity.
 */
function dependency_process_find_dependent_by_rule($entity, $entity_id, $rule, $tag, array &$dependent_entities) {
  $cache_by_rule = &drupal_static('dependency_dependent_ids_by_rule', array());
  $rule_str = serialize($rule);

  if (isset($cache_by_rule[$rule_str])) {
    $type_ids = $cache_by_rule[$rule_str];
    if (empty($type_ids)) {
      return;
    }
    foreach ($type_ids as $next_entity_type => $ids) {
      foreach ($ids as $id) {
        $dependent_entities[$tag][$next_entity_type][$id] = $id;
      }
    }
    return;
  }

  $cache = &drupal_static('dependency_dependent_ids', array());

  $current_entity_ids = array($entity_id);
  $current_entity_id_str = strval($entity_id);
  $next_entity_type = '';
  $next_entity_ids = array();
  $c = count($rule);

  for ($i = 0; ($i + 1) < $c; $i++) {
    $r = $rule[$i];
    $next_r = $rule[$i + 1];
    $current_entity_type = $r['entity'];
    $next_entity_type = $next_r['entity'];
    $next_entity_ids = array();
    $cid = $current_entity_type . ':' . $next_r['entity'] . ':';
    if (isset($r['ref'])) {
      $cid .= $r['ref'];
    }
    elseif (isset($next_r['backref'])) {
      $cid .= 'back-' . $next_r['backref'];
    }
    if (!empty($next_r['bundles'])) {
      $cid .= ':' . implode('-', $next_r['bundles']);
    }

    if (isset($cache[$cid][$current_entity_id_str])) {
      $next_entity_ids = $cache[$cid][$current_entity_id_str];
      if (empty($next_entity_ids)) {
        $cache_by_rule[$rule_str] = array();
        return;
      }
    }
    else {
      if (isset($r['ref'])) {
        if (($i === 0)) {
          dependency_process_get_referenced($current_entity_type, array($entity), $r['ref'], $r['ref_column'], $next_r, $next_entity_ids);
          // Find dependent entities for the previous state
          // of the updated entity.
          if (isset($entity->original)) {
            dependency_process_get_referenced($current_entity_type, array($entity->original), $r['ref'], $r['ref_column'], $next_r, $next_entity_ids);
          }
        }
        else {
          // Do not create huge IN (...) lists in queries
          // and do not use much memory for long entity lists.
          foreach (array_chunk($current_entity_ids, 1000) as $current_entity_id_chunk) {
            $current_entities = dependency_process_get_stub_entities($current_entity_type, $current_entity_id_chunk, $r['ref']);
            dependency_process_get_referenced($current_entity_type, $current_entities, $r['ref'], $r['ref_column'], $next_r, $next_entity_ids);
            unset($current_entities);
          }
        }
      }
      elseif (isset($next_r['backref'])) {
        dependency_process_get_referencing($current_entity_ids, $next_r['backref'], $next_r['backref_column'], $next_r, $next_entity_ids);
      }
      if (empty($next_entity_ids)) {
        $cache[$cid][$current_entity_id_str] = array();
        $cache_by_rule[$rule_str] = array();
        return;
      }
      sort($next_entity_ids, SORT_NUMERIC);
      $cache[$cid][$current_entity_id_str] = $next_entity_ids;
    }

    $current_entity_ids = $next_entity_ids;
    $current_entity_id_str = implode('-', $current_entity_ids);
  }

  if (empty($next_entity_type) || empty($next_entity_ids)) {
    $cache_by_rule[$rule_str] = array();
    return;
  }

  $cache_by_rule[$rule_str] = array($next_entity_type => $next_entity_ids);

  foreach ($next_entity_ids as $id) {
    $dependent_entities[$tag][$next_entity_type][$id] = $id;
  }
}

/**
 * Gets stub entities with one field loaded.
 */
function dependency_process_get_stub_entities($entity_type, $entity_ids, $field_name) {
  $entities = array();

  $field = field_info_field($field_name);
  if (empty($field)) {
    return $entities;
  }

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type);
  $query->entityCondition('entity_id', $entity_ids);
  $result = $query->execute();
  if (!empty($result[$entity_type])) {
    $entities = $result[$entity_type];
  }

  if (empty($entities)) {
    return $entities;
  }

  if (count($entities) <= 100) {
    field_attach_load($entity_type, $entities, FIELD_LOAD_CURRENT, array('field_id' => $field['id']));
  }
  else {
    // Initialize static cache.
    entity_get_info($entity_type);

    $info = &drupal_static('entity_get_info');
    $field_cache = $info[$entity_type]['field cache'];

    // Disable field caches in order to omit cache operations
    // in field_attach_load().
    $info[$entity_type]['field cache'] = FALSE;

    field_attach_load($entity_type, $entities, FIELD_LOAD_CURRENT, array('field_id' => $field['id']));

    // Restore the value in entity_info structure.
    $info[$entity_type]['field cache'] = $field_cache;
  }

  return $entities;
}

/**
 * Gets referenced entity identifiers.
 */
function dependency_process_get_referenced($current_entity_type, array $current_entities, $field_name, $column, array $next_r, array &$next_entity_ids) {
  $next_ids = array();

  foreach ($current_entities as $entity) {
    if (!empty($entity->{$field_name})) {
      foreach ($entity->{$field_name} as $values) {
        foreach ($values as $value) {
          $id = isset($value[$column]) ? $value[$column] : NULL;
          if (!empty($id)) {
            $next_ids[$id] = $id;
          }
        }
      }
    }
  }

  if (empty($next_ids)) {
    return;
  }

  $next_entity_type = $next_r['entity'];

  // Do not create huge IN (...) lists in queries.
  foreach (array_chunk($next_ids, 1000) as $ids) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $next_entity_type);
    if (!empty($next_r['bundles'])) {
      $query->entityCondition('bundle', $next_r['bundles']);
    }
    $query->entityCondition('entity_id', $ids);
    $result = $query->execute();
    if (!empty($result[$next_entity_type])) {
      if (empty($next_entity_ids)) {
        $next_entity_ids = array_keys($result[$next_entity_type]);
      }
      else {
        $next_entity_ids = array_unique(array_merge($next_entity_ids, array_keys($result[$next_entity_type])));
      }
    }
  }
}

/**
 * Gets referencing entity identifiers.
 */
function dependency_process_get_referencing(array $current_entity_ids, $field_name, $column, array $next_r, array &$next_entity_ids) {
  $entity_type = $next_r['entity'];
  $next_entity_ids = array();

  // Don't allow EntityFieldQuery results occupy much memory.
  $limit = 3000;

  // Do not create huge IN (...) lists in queries.
  foreach (array_chunk($current_entity_ids, 1000) as $current_entity_id_chunk) {
    $position = 0;

    while (TRUE) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', $entity_type);
      if (!empty($next_r['bundles'])) {
        $query->entityCondition('bundle', $next_r['bundles']);
      }
      $query->fieldCondition($field_name, $column, $current_entity_id_chunk);
      $query->entityOrderBy('entity_id');
      $query->range($position, $limit);
      $result = $query->execute();

      $ids = !empty($result[$entity_type]) ? array_keys($result[$entity_type]) : array();
      unset($result);

      if (!empty($ids)) {
        $next_entity_ids = empty($next_entity_ids) ? $ids : array_unique(array_merge($next_entity_ids, $ids));
      }

      $position += $limit;

      if (count($ids) < $limit) {
        break;
      }
    }
  }
}

/**
 * Enqueues dependent entities for hook_entity_update() ivoking in cron.
 */
function dependency_process_enqueue_dependent_entities(array $dependent_entities) {
  $queue = dependency_get_queue();
  $queue->createItem($dependent_entities);
}

/**
 * Invokes hook_entity_update() for enqueued dependent entities.
 */
function dependency_process_enqueued_dependent_entities(DrupalQueueInterface $queue) {
  $dependent_entities = array();

  while (TRUE) {
    $item = $queue->claimItem();
    if ($item === FALSE) {
      break;
    }
    foreach ($item->data as $dependent_entity_type => $dependent_entity_ids) {
      foreach ($dependent_entity_ids as $id) {
        $dependent_entities[$dependent_entity_type][$id] = $id;
      }
    }
    $queue->deleteItem($item);
  }

  dependency_process_invoke_entity_update_hooks($dependent_entities);
}

/**
 * Invokes hook_entity_update() for dependent entities.
 */
function dependency_process_invoke_entity_update_hooks(array $dependent_entities) {
  if (empty($dependent_entities)) {
    return;
  }

  $general_entity_update_hook_functions = array();

  foreach (module_implements('entity_update') as $module) {
    if ($module !== 'dependency') {
      $function = $module . '_entity_update';
      if (function_exists($function)) {
        $general_entity_update_hook_functions[] = $function;
      }
    }
  }

  foreach ($dependent_entities as $dependent_entity_type => $dependent_entity_ids) {
    $entity_update_hook = $dependent_entity_type . '_update';

    // Do not load too many entities at once.
    foreach (array_chunk($dependent_entity_ids, 1000) as $ids) {
      $entities = entity_load($dependent_entity_type, $ids);

      foreach ($entities as &$dependent_entity) {
        $entity_clone = clone $dependent_entity;
        $entity_clone->is_new = FALSE;
        $entity_clone->original = $dependent_entity;

        if ($dependent_entity_type === 'user') {
          $edit = array();
          user_module_invoke('update', $edit, $entity_clone, 'account');
        }
        else {
          module_invoke_all($entity_update_hook, $entity_clone);
        }

        foreach ($general_entity_update_hook_functions as $function) {
          call_user_func($function, $entity_clone, $dependent_entity_type);
        }

        unset($entity_clone);
      }

      unset($entities);
      entity_get_controller($dependent_entity_type)->resetCache($ids);
    }
  }
}
