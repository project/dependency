<?php

/**
 * @file
 * Dependency module compiler of dependency rules.
 */

/**
 * Compiles set of rules.
 */
function dependency_rules_compile($text) {
  $lines_by_tag = array();
  $text = str_replace(' ', '', trim($text));

  $rules = array();

  if ($text === '') {
    return $rules;
  }

  $current_tags = array('');

  foreach (explode("\n", $text) as $line) {
    $line = trim($line);
    if (($line === '') || (strpos($line, '#') === 0)) {
      continue;
    }
    $matches = array();
    if (preg_match('/^\[(.*)\]$/', $line, $matches)) {
      if (isset($matches[1])) {
        $current_tags = explode(',', $matches[1]);
      }
      continue;
    }
    $rule = dependency_rules_compile_rule($line);
    if (is_null($rule)) {
      return NULL;
    }
    foreach ($current_tags as $tag) {
      if (!isset($lines_by_tag[$tag][$line])) {
        $rules[$tag][] = $rule;
        $lines_by_tag[$tag][$line] = TRUE;
      }
    }
  }

  return $rules;
}

/**
 * Compiles single rule.
 */
function dependency_rules_compile_rule($str) {
  $str_parts = preg_split('/(->|<-)/', $str, 0, PREG_SPLIT_DELIM_CAPTURE);
  if (empty($str_parts) || (count($str_parts) < 3)) {
    return NULL;
  }
  $rule_parts = array();
  $current_rule_part = NULL;

  foreach ($str_parts as $str_part) {
    if ($str_part === '->') {
      if (empty($current_rule_part) || !empty($current_rule_part['ref']) || !empty($current_rule_part['nextref'])) {
        return NULL;
      }
      $current_rule_part['ref'] = 1;
      continue;
    }
    if ($str_part === '<-') {
      if (empty($current_rule_part) || !empty($current_rule_part['ref']) || !empty($current_rule_part['nextref'])) {
        return NULL;
      }
      $current_rule_part['nextref'] = 1;
      continue;
    }
    $matches = array();
    $bundles = array();
    if (preg_match('/\((.*)\)/', $str_part, $matches)) {
      if (isset($matches[1])) {
        $bundles = explode(',', $matches[1]);
      }
      $str_part = preg_replace('/\(.*\)/', '', $str_part);
    }
    $rule_parts[] = array(
      'str' => $str_part,
      'bundles' => $bundles,
    );
    $current_rule_part = &$rule_parts[count($rule_parts) - 1];
  }

  $c = count($rule_parts);

  if ($c < 2) {
    return NULL;
  }

  $fields = field_info_field_map();

  for ($i = 0; $i < $c; $i++) {
    $r = &$rule_parts[$i];

    if (!empty($r['nextref'])) {
      if ($i + 1 === $c) {
        return NULL;
      }
      unset($r['nextref']);
      $rule_parts[$i + 1]['backref'] = 1;
    }
    if (!empty($r['ref']) && ($i + 1 === $c)) {
      return NULL;
    }

    $parts = explode(':', $r['str']);
    unset($r['str']);

    if (!empty($r['ref']) && !empty($r['backref'])) {
      if (count($parts) !== 3) {
        return NULL;
      }
      $r['backref'] = $parts[0];
      $r['entity'] = $parts[1];
      $r['ref'] = $parts[2];
    }
    elseif (!empty($r['ref']) && empty($r['backref'])) {
      if (count($parts) !== 2) {
        return NULL;
      }
      $r['entity'] = $parts[0];
      $r['ref'] = $parts[1];
    }
    elseif (empty($r['ref']) && !empty($r['backref'])) {
      if (count($parts) !== 2) {
        return NULL;
      }
      $r['backref'] = $parts[0];
      $r['entity'] = $parts[1];
    }
    else {
      if (count($parts) !== 1) {
        return NULL;
      }
      $r['entity'] = $parts[0];
    }

    $entity_info = entity_get_info($r['entity']);
    if (empty($entity_info)) {
      return NULL;
    }
    if (!empty($r['bundles'])) {
      if (empty($entity_info['bundles'])) {
        return NULL;
      }
      foreach ($r['bundles'] as $bundle) {
        if (!isset($entity_info['bundles'][$bundle])) {
          return NULL;
        }
      }
    }
    else {
      unset($r['bundles']);
    }

    foreach (array('ref', 'backref') as $ref_name) {
      if (isset($r[$ref_name])) {
        $f = $r[$ref_name];
        if (!isset($fields[$f]) || !isset($fields[$f]['bundles'][$r['entity']])) {
          return NULL;
        }
        if ($fields[$f]['type'] === 'entityreference') {
          $r[$ref_name . '_column'] = 'target_id';
        }
        elseif ($fields[$f]['type'] === 'taxonomy_term_reference') {
          $r[$ref_name . '_column'] = 'tid';
        }
        else {
          return NULL;
        }
      }
    }
  }

  return $rule_parts;
}
