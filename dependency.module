<?php

/**
 * @file
 * Dependency module hook implementations.
 */

/**
 * Implements hook_menu().
 */
function dependency_menu() {
  $items = array();

  $items['admin/config/system/dependency'] = array(
    'title' => 'Entity dependencies',
    'description' => 'Configure entity dependencies.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dependency_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/dependency.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_cron().
 */
function dependency_cron() {
  $queue = dependency_get_queue();
  if ($queue->numberOfItems() > 0) {
    module_load_include('inc', 'dependency', 'includes/dependency.process');
    dependency_process_enqueued_dependent_entities($queue);
  }
}

/**
 * Implements hook_entity_insert().
 */
function dependency_entity_insert($entity, $type) {
  if (!dependency_in_dependency_processing() && !dependency_processing_disabled()) {
    dependency_process_entity_operation($type, $entity, 'insert');
  }
}

/**
 * Implements hook_entity_update().
 */
function dependency_entity_update($entity, $type) {
  if (!dependency_in_dependency_processing() && !dependency_processing_disabled()) {
    dependency_process_entity_operation($type, $entity, 'update');
  }
}

/**
 * Implements hook_entity_delete().
 */
function dependency_entity_delete($entity, $type) {
  if (!dependency_in_dependency_processing() && !dependency_processing_disabled()) {
    dependency_process_entity_operation($type, $entity, 'delete');
  }
}

/**
 * Implements hook_features_post_restore().
 */
function dependency_features_post_restore($op, $items) {
  cache_clear_all('dependency:rules', 'cache');
}

/**
 * Gets dependency rules and applies them.
 */
function dependency_process_entity_operation($entity_type, $entity, $operation) {
  static $rules = NULL;

  if (is_null($rules)) {
    $cache = cache_get('dependency:rules');
    if (empty($cache)) {
      module_load_include('inc', 'dependency', 'includes/dependency.compile');
      $rules = dependency_rules_compile(variable_get('dependency_rules', ''));
      if (is_null($rules)) {
        $rules = array();
      }

      cache_set('dependency:rules', $rules);

      if (empty($rules)) {
        return;
      }
    }
    else {
      $rules = $cache->data;
    }
  }

  if (empty($rules)) {
    return;
  }

  dependency_in_dependency_processing(TRUE);
  module_load_include('inc', 'dependency', 'includes/dependency.process');
  dependency_process_apply_rules($entity_type, $entity, $rules, $operation);
  dependency_in_dependency_processing(FALSE);
}

/**
 * Disable dependency processing.
 *
 * This can be called before massive updates of entities
 * if single entity processing is not required.
 *
 * @param bool $persistent
 *   TRUE if processing must be disabled for multiple requests.
 */
function dependency_disable_processing($persistent = FALSE) {
  dependency_processing_disabled(TRUE, $persistent);
}

/**
 * Enable dependency processing.
 *
 * This can be called after massive updates of entities
 * to enable single entity processing again.
 *
 * @param bool $persistent
 *   TRUE if processing was disabled for multiple requests.
 */
function dependency_enable_processing($persistent = FALSE) {
  dependency_processing_disabled(FALSE, $persistent);
}

/**
 * Returns TRUE if dependency processing is disabled.
 */
function dependency_processing_disabled($state = NULL, $persistent = FALSE) {
  if (!is_null($state)) {
    if ($state) {
      $GLOBALS['_dependency_processing_disabled'] = TRUE;
    }
    else {
      unset($GLOBALS['_dependency_processing_disabled']);
    }
    if ($persistent) {
      variable_set('dependency_processing_disabled', $state);
    }
  }

  return !empty($GLOBALS['_dependency_processing_disabled']) || variable_get('dependency_processing_disabled', FALSE);
}

/**
 * Returns TRUE if dependency processing is active.
 */
function dependency_in_dependency_processing($state = NULL) {
  if (!is_null($state)) {
    if ($state) {
      $GLOBALS['_dependency_processing'] = TRUE;
    }
    else {
      unset($GLOBALS['_dependency_processing']);
    }
  }

  return !empty($GLOBALS['_dependency_processing']);
}

/**
 * Returns queue for cron processing.
 *
 * @return DrupalQueueInterface
 *   Drupal queue storing dependent entities to process.
 */
function dependency_get_queue() {
  return DrupalQueue::get('dependency_dependent_entities', TRUE);
}
