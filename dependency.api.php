<?php

/**
 * @file
 * Hooks provided by Dependency module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * The dependent entities are found.
 *
 * This hook is called after some entity was inserted / updated / deleted
 * and dependent entities are found using dependency rules.
 *
 * @param string $dependent_entity_type
 *   The entity type of dependent entities.
 * @param array $dependent_entity_ids
 *   The array of dependent entity identifiers.
 * @param string $tag
 *   The tag associated with dependency rules.
 * @param array $context
 *   The context of the initial entity operation.
 *   An associative array with the following keys:
 *   - 'entity_type'
 *   - 'entity'
 *   - 'operation' - one of 'insert', 'update', 'delete'.
 */
function hook_dependent_entity_notify($dependent_entity_type, $dependent_entity_ids, $tag, $context) {
  if ($tag === 'search_index') {
    // Reindex entities.
    search_api_track_item_change($dependent_entity_type, $dependent_entity_ids);
  }
}

/**
 * @} End of "addtogroup hooks".
 */
